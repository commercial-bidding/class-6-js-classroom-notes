'use strict';

function area_of_circle(r,pi){
    //**先用关键字arguments 判断一下输入的参数个数**
    if(arguments.length === 1){
        var pi =3.14; //如果没有输入pi的值，就会给pi赋值
    }
      return r*r*pi;//返回计算结果
}
//测试:
if(area_of_circle(2)=== 12.56 && area_of_circle(2,3.1416)=== 12.5654){
    console.log('测试通过');
}else{
    console.log('测试通过');
}