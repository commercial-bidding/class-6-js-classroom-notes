'use strict';
$(document).ready(function () {
    let checkAll=$('.selectAll>input[type=checkbox]');//全选全不选复选框
    let checkitems=$('[name=lang]');//语言选择
    let a=$('.invertSelect');  //反选
    let s1=$('span.selectAll');
    let s2=$('span.deselectAll');
    s2.text('');

    checkAll.on("click",function () {
        checkitems.prop("checked",checkAll.prop("checked"));
        updateCheckAll();
    });

    a.on("click", function() {
        checkitems.each(function() {
          $(this).prop("checked", !$(this).prop("checked"));
        });
        updateCheckAll();
    });
      
    function updateCheckAll() {
        let allChecked = true;
        let allUnchecked = true;
        checkitems.each(function () {
            if ($(this).prop('checked')) {
                allUnchecked=false;
            }else{
                allChecked=false;
            }
        });
        if(allChecked){
            checkAll.prop('checked',true);
            s1.text('全不选');
        }else if(allUnchecked){
            checkAll.prop('checked',false);
            s1.text('全选');
        }else{
            checkAll.prop('checked',false);
            s1.text('全选');
        }
    }

    checkitems.on("click", function() {
        updateCheckAll();
    });
});