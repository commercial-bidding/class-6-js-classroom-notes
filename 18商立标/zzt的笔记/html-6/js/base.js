'use strict';

function btnSave() {

}

function btnAdd() {
    location.href = './edit.html';
}
function btnEdit() {
    location.href = './edit.html';
}

$('[value=删除]').click(btnDel);
$('[value=编辑]').click(btnEdit);

function btnDel(e) {
    // 拿到当前事件发生的节点
    let currentNode = e.currentTarget;
    // 拿到节点后，观察dom节点结构，发现我们要删除的行，要上两层，所以，就调用了两次parent()方法,最后调用remove方法移除
    let row = $(currentNode).parent().parent();

    let firstTD = row.find('td').first().text();

    // console.log(firstTD);

    let isOk = confirm(`确定要删除id为:${firstTD}所在的行吗？`);
    // let isOk = confirm('确定要删除id为:' + firstTD + '所在的行吗？');
    // 当用户点击确定删除的时候，执行如下业务
    if (isOk) {
        row.remove();

        // console.log(currentNode);
        // console.log($(currentNode).parent());
        // console.log($(currentNode).parent().parent().remove());
    }


}

